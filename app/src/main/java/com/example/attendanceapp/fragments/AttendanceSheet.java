package com.example.attendanceapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.example.attendanceapp.adapters.SheetAdapter;
import com.example.attendanceapp.models.SheetModel;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

/**
 * This fragment implements individual Attendance SHEET list. Its done using the FirestoreRecyclerOptions. Essentailly we pass in a query/reference for a collection to
 * FirestoreRecyclerOptions and a data model for the documents and it populates a recycleview based on the adapter class for it. The adapter class used here is
 * our SheetAdapter and SheetModel. This fragment is only seen by the teacher
 */
public class AttendanceSheet extends Fragment {

    private RecyclerView recyclerView;
    private SheetAdapter sheetAdapter;
    private FirestoreServer server;
    private String sheetId;
    private String classId;
    DocumentReference documentReference;

    //Most things here are similar to the CurrentClassFragment and PastClassFragments which are more commented. Differences are noted
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_attendance_sheet, container, false);
        server = FirestoreServer.getInstance();
        recyclerView = view.findViewById(R.id.attendance_sheet_recycler);
        //we retrieve a course id and a sheet id to know which class and sheet we're creating/inflating. Comes from Attendance Sheets Fragment
        classId = getArguments().getString("CClass");
        sheetId = getArguments().getString("Sheet");
        documentReference = server.getAttendanceSheet(classId, sheetId);
        CollectionReference ref = documentReference.collection("All Students");


        FirestoreRecyclerOptions<SheetModel> options = new FirestoreRecyclerOptions.Builder<SheetModel>()
                .setQuery(ref, SheetModel.class).build();
        sheetAdapter = new SheetAdapter(options);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(sheetAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        sheetAdapter.OnsetSheetClick(new SheetAdapter.OnSheetClick() {
            //different onClick method. Sets the students Attended bool to true
            @Override
            public void sheetClick(DocumentSnapshot documentSnapshot, int position) {
                server.updateAttendanceSheet(documentSnapshot);

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        sheetAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        sheetAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        //when you leave this fragment you are no longer taking attendance so set this to false
        server.takingAttendance(documentReference, false);
        if (sheetAdapter != null) {
            sheetAdapter.stopListening();
        }
    }
}