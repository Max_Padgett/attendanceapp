package com.example.attendanceapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.example.attendanceapp.activities.StudentCheck;
import com.example.attendanceapp.activities.TeacherAttendance;
import com.example.attendanceapp.adapters.CourseAdapter;
import com.example.attendanceapp.models.CourseModel;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


/**
 * This fragment implements the current class list. Its done using the FirestoreRecyclerOptions. Essentailly we pass in a query/reference for a collection to
 * FirestoreRecyclerOptions and a data model for the documents and it populates a recycleview based on the adapter class for it. The adapter class used here is
 * our course adapter. This fragment is shared by both teachers and students and its onClick methods and references are different depending on which type.
 */
public class CurrentClassFragment extends Fragment {

    RecyclerView recyclerView;
    FirebaseFirestore db;
    FirebaseAuth firebaseAuth;
    View view;
    public CourseAdapter adapter;
    private FirestoreServer server;
    private CollectionReference ref;
    private CurrentClassListener mListener;

    public CurrentClassFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_current_class, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        server = FirestoreServer.getInstance();

        if(server.getUser().equals("Student")) //This is our server singleton we use to store the type of user when they log in. Determines which collection they have.
            ref = server.getStudentCurrentClasses();
        else
            ref = server.getTeacherCurrentClasses();

        //Our Builder for our recycler view. pass in data model and reference.
        FirestoreRecyclerOptions<CourseModel> options = new FirestoreRecyclerOptions.Builder<CourseModel>()
                .setQuery(ref, CourseModel.class).build();

        //pass options into adapter. And set up adapter in certain ways.
        adapter = new CourseAdapter(options);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        /*Onclick listeners depending on the type of user.*/
        if(server.getUser().equals("Student")){
            adapter.OnSetItemClick(new CourseAdapter.OnItemClick() {

                //sends the student to the check in activity and passes a string for path in the database.
                @Override
                public void itemClick(DocumentSnapshot documentSnapshot, int position) {

                    String id = documentSnapshot.getId();
                    String path = documentSnapshot.getReference().getPath();
                    Intent intent = new Intent(getContext(), StudentCheck.class);
                    intent.putExtra("Sid",id);
                    intent.putExtra("Sclass", path);
                    startActivity(intent);
                    Toast.makeText(getActivity(),
                            "Position: " + position + " ID: " + id, Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            adapter.OnSetItemClick(new CourseAdapter.OnItemClick() {

                //sends the teacher to the attendance sheet activity and passes a string for the id of the course in the database. (i hadn't figured out path at the time)
                @Override
                public void itemClick(DocumentSnapshot documentSnapshot, int position) {
                    String id = documentSnapshot.getId();
                    String path = documentSnapshot.getReference().getPath();
                    Intent intent = new Intent(getContext(), TeacherAttendance.class);
                    intent.putExtra("CClass", id);
                    startActivity(intent);
                }
            });
        }


        // Inflate the layout for this fragment
        return view;
    }


    //required for recycler view to update
    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    public interface CurrentClassListener{
        void onListChanged(CourseAdapter courseAdapter);
    }
    public void setOnCurrentClassChangeListener(CurrentClassListener listener){
        this.mListener = listener;
    }
    //required for recycler view to populate
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    //required for recycler view to stop populate. tells it to stop when fragment is out of view.
    @Override
    public void onStop() {
        super.onStop();

        if (adapter != null) {
            adapter.stopListening();
        }
    }
}
