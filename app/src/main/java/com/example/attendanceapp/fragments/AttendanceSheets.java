package com.example.attendanceapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.example.attendanceapp.adapters.AttendanceAdapter;
import com.example.attendanceapp.models.AttendanceModel;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

/**
 * This fragment implements a list of Attendance Sheets. Its done using the FirestoreRecyclerOptions. Essentailly we pass in a query/reference for a collection to
 * FirestoreRecyclerOptions and a data model for the documents and it populates a recycleview based on the adapter class for it. The adapter class used here is
 * our AttendanceAdapter and AttendanceModel. This fragment is only seen by the teacher.
 */
public class AttendanceSheets extends Fragment {

    String id;
    RecyclerView recyclerView;
    FirestoreServer server;
    CollectionReference ref;
    AttendanceAdapter attendanceAdapter;

    //Most things here are similar to the CurrentClassFragment and PastClassFragments which are more commented. Differences are noted
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_attendance_sheets, container, false);
        recyclerView = view.findViewById(R.id.attendance_sheets_recycler);

        server = FirestoreServer.getInstance();

        //getting the class we want our attendance sheets from. This comes from the CurrentClassFragment
        id = getArguments().getString("CClass");
        DocumentReference documentReference = server.getCurrentClass(id);
        ref = server.getAttendanceSheets(documentReference);

        FirestoreRecyclerOptions<AttendanceModel> options = new FirestoreRecyclerOptions.Builder<AttendanceModel>()
                .setQuery(ref, AttendanceModel.class).build();

        attendanceAdapter = new AttendanceAdapter(options);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(attendanceAdapter);

        //On Click listener gives it to another fragment called Attendance Sheet. Passes course and which attendance sheet.
        attendanceAdapter.OnsetAttendanceClick(new AttendanceAdapter.OnAttendanceClick() {
            @Override
            public void attendanceClick(DocumentSnapshot documentSnapshot, int position) {
                Bundle bundle = new Bundle();
                bundle.putString("CClass", id);
                bundle.putString("Sheet", documentSnapshot.getId());
                AttendanceSheet attendanceSheet = new AttendanceSheet();
                attendanceSheet.setArguments(bundle);
                server.takingAttendance(documentSnapshot, true);            //this method sets a bool for taking attendance on a particular sheet
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_layout_attendance, attendanceSheet, "findfrag")
                        .addToBackStack(null)
                        .commit();
            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        attendanceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        attendanceAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (attendanceAdapter != null) {
            attendanceAdapter.stopListening();
        }
    }
}