package com.example.attendanceapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.example.attendanceapp.activities.TeacherAttendance;
import com.example.attendanceapp.adapters.CourseAdapter;
import com.example.attendanceapp.models.CourseModel;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


/**
 * This fragment implements the past class list. Its done using the FirestoreRecyclerOptions. Essentailly we pass in a query/reference for a collection to
 * FirestoreRecyclerOptions and a data model for the documents and it populates a recycleview based on the adapter class for it. The adapter class used here is
 * our courseadapter and CourseModel. This fragment is shared by both teachers and students and its onClick methods and references are different depending on which type.
 */
public class PastClassFragment extends Fragment {

    private RecyclerView recyclerView;
    private FirebaseFirestore db;
    private FirebaseAuth firebaseAuth;
    private View view;
    private LinearLayoutManager linearLayoutManager;
    private CourseAdapter adapter;
    private CollectionReference ref;
    private FirestoreServer server;

    public PastClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_past_class, container, false);
        recyclerView = view.findViewById(R.id.recyclerView2);

        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        server = FirestoreServer.getInstance();

        //This is our server singleton we use to store the type of user when they log in. Determines which collection and methods they have.
        if(server.getUser().equals("Student"))
            ref = server.getStudentPastClasses();
        else
            ref = server.getTeacherPastClasses();

        //Our Builder for our recycler view. pass in data model and reference.
        FirestoreRecyclerOptions<CourseModel> options = new FirestoreRecyclerOptions.Builder<CourseModel>()
                .setQuery(ref, CourseModel.class).build();

        //pass options into adapter. And set up adapter in certain ways.
        adapter = new CourseAdapter(options);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager( linearLayoutManager);
        recyclerView.setAdapter(adapter);

        /*Onclick listeners depending on the type of user.*/
        if(server.getUser().equals("Student")){
            adapter.OnSetItemClick(new CourseAdapter.OnItemClick() {

                //we don't want students to change past classes list really so just a toast
                @Override
                public void itemClick(DocumentSnapshot documentSnapshot, int position) {
                    String id = documentSnapshot.getId();
                    Toast.makeText(getActivity(),
                            "Position: " + position + " ID: " + id, Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            adapter.OnSetItemClick(new CourseAdapter.OnItemClick() {

                //Teachers can edit and modify past classes.
                @Override
                public void itemClick(DocumentSnapshot documentSnapshot, int position) {
                    String id = documentSnapshot.getId();
                    String path = documentSnapshot.getReference().getPath();
                    Intent intent = new Intent(getContext(), TeacherAttendance.class);
                    intent.putExtra("CClass", id);
                    startActivity(intent);
                }
            });
        }


        // Inflate the layout for this fragment
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (adapter != null) {
            adapter.stopListening();
        }
    }
}
