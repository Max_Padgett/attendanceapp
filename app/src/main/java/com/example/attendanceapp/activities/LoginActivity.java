package com.example.attendanceapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
/* This is our login activity.
* */
public class LoginActivity extends AppCompatActivity {
    Button logoutBtn, newUser, loginBtn; //Buttons for logging out, creating new user, and logging in
    EditText mEmail, mPassword;         //email password
    TextView mCurrUser;                 //rarely seen. Just says current user above title. Was for debuging
    FirebaseAuth firebaseAuth;          //instance of Firebase Auth which we use here
    FirestoreServer server;             //instance of our created firestore server class. Which we use to save methods and data that are useful
    FirebaseFirestore db;               //Actual instance of firebasefirestore server.
    ProgressBar progressBar;            //the progressbar

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressBar = findViewById(R.id.progress_circular_login);
        logoutBtn = findViewById(R.id.logoutBtn);
        newUser = findViewById(R.id.newUserBtn);
        loginBtn = findViewById(R.id.loginButton);
        firebaseAuth = FirebaseAuth.getInstance();
        mEmail = findViewById(R.id.loginEdit);
        mPassword = findViewById(R.id.loginpassEdit);
        mCurrUser = findViewById(R.id.current_userTextView);
        db = FirebaseFirestore.getInstance();
        server = FirestoreServer.getInstance();

        firebaseAuth.signOut();                                 //this can be commented out if you want to have it auto sign in

        //This makes it possible to auto sign in
        if(firebaseAuth.getCurrentUser() != null){
            Toast.makeText(LoginActivity.this, "User is: " + firebaseAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
            mCurrUser.setText("Logged in as:" + firebaseAuth.getCurrentUser().getEmail());
            login();
        }

        //This would sign you out if you somehow were signed in.
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(LoginActivity.this, "User Signed Out.", Toast.LENGTH_SHORT).show();
            }
        });
        //Button for register activity
        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });
        //Button for logging in
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);                            //Let the user know progress is being made
                String email = mEmail.getText().toString().trim();                  //get email and password
                String password = mPassword.getText().toString().trim();            //get email and password
                if(TextUtils.isEmpty(email)){                                       //simple error checks
                    mEmail.setError("Email is required!");
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    mPassword.setError("Password is required!");
                    return;
                }
                if(password.length() < 6){
                    mPassword.setError("Password must be 6 or more characters long!");
                    return;
                }

                //method for signing in with firebase Auth. Pass in email and pass
                firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    //Listener. This is an Async Task.
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //If we're successful login with login method
                        if(task.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "User Logged In", Toast.LENGTH_SHORT).show();
                            login();
                        }
                        else{
                            //else toast and set progress bar to invisible
                            Toast.makeText(LoginActivity.this, "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
        });

    }
    //Our login method. This does two async tasks to determine which type of user you are. It checks the student database first
    //to see if your a student then the teacher database. Will set the FirestoreSever class's user field to whichever for use later in code.
    public void login(){

        server.getStudent().get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()){
                        server.setUser("Student");
                        startActivity(new Intent(getApplicationContext(), StudentActivity.class));
                        finish();
                    }else{
                        Log.d("Not", "Not a student");
                    }
                }else {
                    Log.d("Failed", "Student task failed");
                }

            }
        });
        server.getTeacher().get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if(documentSnapshot.exists()){
                        server.setUser("Teacher");
                        startActivity(new Intent(getApplicationContext(), TeacherActivity.class));
                        finish();
                    }else{
                        Log.d("Not", "Not a teacher");
                    }
                }else
                {
                    Log.d("Failed", "Teacher task failed");
                }
            }
        });

    }
}
