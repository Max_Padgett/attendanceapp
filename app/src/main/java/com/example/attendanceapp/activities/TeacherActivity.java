package com.example.attendanceapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.example.attendanceapp.R;
import com.example.attendanceapp.adapters.ViewPagerFragmentAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

/*This activity and Student activity are very similar. Here we set up the Floating Action Button and the tabs for CurrentClassFragment and PastClassFragment.
 * The FAB actually starts the AddingActivity for adding more courses to the teachers CurrentClasses.
 * */
public class TeacherActivity extends AppCompatActivity {

    TabLayout mTabLayout;
    ViewPager2 viewPager;
    ViewPagerFragmentAdapter adapter;
    FloatingActionButton fab;
    public static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = findViewById(R.id.viewpager);
        mTabLayout = findViewById(R.id.tabLayout);
        fab = findViewById(R.id.fab_add);


        adapter = new ViewPagerFragmentAdapter(getSupportFragmentManager(), getLifecycle());
        adapter.createFragment(0);
        adapter.createFragment(1);
        viewPager.setOrientation(ViewPager2.ORIENTATION_VERTICAL);
        viewPager.setAdapter(adapter);
        new TabLayoutMediator(mTabLayout, viewPager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override
                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                        if(position ==0)
                            tab.setText("Current Courses");
                        if (position ==1)
                            tab.setText("Past Courses");
                    }
                }
        ).attach();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TeacherActivity.this, AddingActivity.class);

                startActivityForResult(intent, REQUEST_CODE);
            }

        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {


        }catch (Exception e){

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
