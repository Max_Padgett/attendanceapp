package com.example.attendanceapp.activities;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;

import com.example.attendanceapp.fragments.AttendanceSheets;
import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
/*This activity really just handles two fragments the AttendanceSheets and AttendanceSheet Fragments. It also has a fab for adding new attendance sheets*/
public class TeacherAttendance extends FragmentActivity {

    FloatingActionButton fab;
    FirestoreServer server;
    DocumentReference ref;
    String id;

    public TeacherAttendance() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_attendance);
        if(findViewById(R.id.frame_layout_attendance) != null){
            if(savedInstanceState != null){
                return;
            }
            server = FirestoreServer.getInstance();
            id = getIntent().getStringExtra("CClass");
            ref = server.getCurrentClass(id);

            fab = findViewById(R.id.fab_attendance);

            //calls our created method in our FirestoreServer Class
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    server.addAttendanceSheet(ref);
                }

            });

            //Just starts the Attendance Sheets frag.
            AttendanceSheets firstFrag = new AttendanceSheets();
            firstFrag.setArguments(getIntent().getExtras());


            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_layout_attendance, firstFrag).commit();


        }

    }
}