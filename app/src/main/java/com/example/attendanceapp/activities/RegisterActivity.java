package com.example.attendanceapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

/*This is our RegisterActivity. Its not that complex actually. It grabs a bunch of input data and uses that to call two firebase/firestore methods at the bottom.*/
public class RegisterActivity extends AppCompatActivity {

    EditText mFname,mLname, mEmail, mPass, mRePass;
    Button regBtn, alreadyBtn, finalBtn;
    FirebaseAuth fAuth;
    ProgressBar progressBar;
    String email, password, repass, Fname, Lname;
    Integer type;
    RadioGroup radioGroup;
    FirebaseFirestore db;
    FirestoreServer server;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFname = findViewById(R.id.fnameEdit);
        mLname = findViewById(R.id.lnameEdit);
        mEmail = findViewById(R.id.emailEdit);
        mPass = findViewById(R.id.passEdit);
        mRePass = findViewById(R.id.repassEdit);
        regBtn = findViewById(R.id.regButton);
        alreadyBtn = findViewById(R.id.alreadyButton);
        progressBar = findViewById(R.id.regProgBar);
        radioGroup = findViewById(R.id.radioGroup);
        fAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        server = FirestoreServer.getInstance();

        //not really useful at the current time.
        /*if(fAuth.getCurrentUser() != null){
            Toast.makeText(RegisterActivity.this, "User is:" + fAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }*/
        //just sends you back
        alreadyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        //grabs the data and does a bunch of checks. Then puts that data in a HashMap to send to the Firestore server (not the class).
        //
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 email = mEmail.getText().toString().trim();
                 password = mPass.getText().toString().trim();
                 repass = mRePass.getText().toString().trim();
                 Fname = mFname.getText().toString().trim();
                 Lname = mLname.getText().toString().trim();
                if(TextUtils.isEmpty(email)){
                    mEmail.setError("Email is required!");
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    mPass.setError("Password is required!");
                    return;
                }
                if(TextUtils.isEmpty(repass) || (!repass.equals(password))){
                    mPass.setError("Passwords do not match !");
                    return;
                }
                if(TextUtils.isEmpty(Fname)){
                    mFname.setError("First Name is required!");
                    return;
                }
                if(TextUtils.isEmpty(Lname)){
                    mLname.setError("Last Name is required!");
                    return;
                }
                if(password.length() < 6){
                    mPass.setError("Password must be 6 or more characters long!");
                    return;
                }
                if(radioGroup.getCheckedRadioButtonId() == -1)
                {
                    Toast.makeText(getApplicationContext(), "Please select type", Toast.LENGTH_SHORT).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                type = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = findViewById(type);
                String collectionName = (String) radioButton.getText();
                //putting data in Map
                Map<String, Object> user = new HashMap<>();
                user.put("firstName", Fname);
                user.put("lastName", Lname);
                user.put("email", email);

                //creating user with email and password. A firebaseAuth function.
                fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(RegisterActivity.this, "User Created", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Log.d("Auth", "Error: " + task.getException().getMessage());
                        }
                    }
                });
                //This is more complex. It essentially places the user in the database based on the type of user their registering as.
                //Thier email is thier unique id as there is a database rule of 1 email per 1 user so they're unique.
                db.collection("Texas A&M University - Corpus Christi").document(collectionName +"s").collection(collectionName + "s")
                        .document(email)
                                .set(user)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void adVoid) {
                                    Toast.makeText(RegisterActivity.this, "User Auth Created", Toast.LENGTH_SHORT).show();
                                    server.setUser(collectionName);
                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("db", "Error: " + e.getMessage());
                            finish();
                        }
                    });
            }
        });
    }
}
