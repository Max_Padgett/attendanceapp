package com.example.attendanceapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.attendanceapp.R;
import com.example.attendanceapp.activities.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;

//not really used. Time did not allow it. Not reachable from program.
public class MainActivity extends AppCompatActivity {

    Button logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logoutBtn = findViewById(R.id.logbtn);
        Spinner dropdown = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter =  ArrayAdapter.createFromResource(this,R.array.Course_array, android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(adapter);
        Spinner dropdown2 = findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter2 =  ArrayAdapter.createFromResource(this,R.array.Year_array, android.R.layout.simple_spinner_dropdown_item);
        dropdown2.setAdapter(adapter2);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });
    }
}
