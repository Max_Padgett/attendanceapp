package com.example.attendanceapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/*This activity implements the student check in system. I.E. when a student presses the im here button the teacher is notified on the attendance sheet
 with a yellow circle. This is done by setting the checked field in the Attendance Sheets/All Students/%student email% path to true when the button is clicked*/
public class StudentCheck extends AppCompatActivity {
    TextView textView;
    Button imHere;
    FirestoreServer server;
    FirebaseFirestore db;
    String sref;
    DocumentReference ref;
    FirebaseAuth auth;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_check);
        textView = findViewById(R.id.checkText);
        server = FirestoreServer.getInstance();
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        imHere = findViewById(R.id.imhere);
        sref = getIntent().getStringExtra("Sclass");    //intent contains the path to the class the student clicked on
        id = getIntent().getStringExtra("Sid");
        ref = db.document(sref);
        textView.setText(id);

        imHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                server.studentCheck(ref, auth.getCurrentUser().getEmail());
            }
        });
    }
}