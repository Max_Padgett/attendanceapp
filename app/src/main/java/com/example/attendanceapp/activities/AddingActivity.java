package com.example.attendanceapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.attendanceapp.fragments.CurrentClassFragment;
import com.example.attendanceapp.FirestoreServer;
import com.example.attendanceapp.R;
import com.example.attendanceapp.adapters.CourseAdapter;
import com.example.attendanceapp.models.CourseModel;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

/*This activity allows the user to add classes to their current classes. In the database it copies data from the Courses/Current Courses collection to
* the Student/Current Courses or the Teacher/Current Courses*/
public class AddingActivity extends AppCompatActivity implements CurrentClassFragment.CurrentClassListener {

    RecyclerView recyclerView;
    FirebaseFirestore db;
    FirebaseAuth firebaseAuth;
    View view;
    private LinearLayoutManager linearLayoutManager;
    private FirestoreServer server;
    private CourseAdapter adapter;


    public AddingActivity() {
        // Required empty public constructor
    }

    //the onCreate method is extremely similar to CurrentClassFragment and PastClassFragment.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding);
        recyclerView = findViewById(R.id.recyclerView3);
        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        view = findViewById(R.id.adding_view);
        server = FirestoreServer.getInstance();

        CollectionReference ref = server.getCurrentCourses();


        Log.d("collection", ref.toString());
        FirestoreRecyclerOptions<CourseModel> options = new FirestoreRecyclerOptions.Builder<CourseModel>()
                .setQuery(ref, CourseModel.class).build();

        adapter = new CourseAdapter(options);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        //here i used a different method to differentiate
        adapter.OnSetItemClick(new CourseAdapter.OnItemClick() {
            @Override
            public void itemClick(DocumentSnapshot documentSnapshot, int position) {
                if(server.getUser().equals("Student")){
                    server.addStudentCourse(documentSnapshot);
                    Snackbar.make(view, "Added " + documentSnapshot.getId(), Snackbar.LENGTH_SHORT).show();
                }
                else
                {
                    server.addTeacherCourse(documentSnapshot);
                    Snackbar.make(view, "Added " + documentSnapshot.getId(), Snackbar.LENGTH_SHORT).show();
                }

                finish();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (adapter != null) {
            adapter.stopListening();
        }
    }

    @Override
    public void onListChanged(CourseAdapter courseAdapter) {
        courseAdapter.notifyDataSetChanged();
    }


}
