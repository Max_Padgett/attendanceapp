package com.example.attendanceapp.models;

/*I'll just explain how the database works here. The Firestore database is a No SQL database managed via Documents and Collections (of Documents)
 * This class models a specific type of document in this case an individual students document in a subcollection of the attendance sheet document.
 * This SheetModel class is used to model list items in the AttendanceSheet  fragment.
 * */
public class SheetModel {
    String firstName;
    String lastName;
    String email;
    Boolean Attended;
    Boolean Checked;

    public SheetModel(){

    }
    public SheetModel(String firstName, String lastName, String email, Boolean attended, Boolean checked){
        this.firstName = firstName;
        this.lastName = lastName;
        this.Attended = attended;
        this.email = email;
        this.Checked = checked;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAttended() {
        return Attended;
    }

    public void setAttended(Boolean attended) {
        Attended = attended;
    }

    public Boolean getChecked() {
        return Checked;
    }

    public void setChecked(Boolean checked) {
        Checked = checked;
    }


}
