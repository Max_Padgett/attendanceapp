package com.example.attendanceapp.models;

import com.google.firebase.firestore.ServerTimestamp;
import java.util.Date;
import java.util.List;


/*I'll just explain how the database works here. The Firestore database is a No SQL database managed via Documents and Collections (of Documents)
 * This class models a specific type of document in this case a Course's data. Most of the data isn't used do to time constraints but its there.
 * This coursemodel class is used to model list items in the CurrentCoursesFragment and PastCoursesFragment. As well as the Adding Activity
 * */
public class CourseModel {
    private String Name;
    private String Department;
    private Integer Number;
    private String Section;
    private Date startDate;
    private Date endDate;



    private List<String> time;
    public CourseModel(){}
    public CourseModel(String courseName, String Department, Integer Number, String Section) {
        this.Name = courseName;
        this.Department = Department;
        this.Number = Number;
        this.Section = Section;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }
    public String getName() {return Name;}


    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public Integer getNumber() {
        return Number;
    }

    public void setNumber(Integer number) {
        Number = number;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }
}
