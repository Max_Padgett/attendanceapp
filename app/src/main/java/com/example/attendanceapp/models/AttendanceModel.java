package com.example.attendanceapp.models;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.ArrayList;
import java.util.Date;

/*I'll just explain how the database works here. The Firestore database is a No SQL database managed via Documents and Collections (of Documents)
* This class models a specific type of document in this case an attendance sheets data so its creation date which we use as its unique id.
* This document actually has a subcollection that we model in SheetModel. But this allows us to create a list of attendance sheets in TeacherAttendance Activity and
* Attendance Sheets fragment. The ArrayList's are not important but I thought we might use on the client later. Turns out the better way was using a subcollection
* rather than arrays on the documents themselves.
* */
public class AttendanceModel {
    @ServerTimestamp
    private Date id;
    private ArrayList<String> students;
    private ArrayList<String> attended;

    public AttendanceModel(){}
    public AttendanceModel(Date id, ArrayList<String> students, ArrayList<String> attended) {
        this.id = id;
        this.students = students;
        this.attended = attended;

    }

    public Date getId() {
        return id;
    }
    public void setId(Date id) {
        this.id = id;
    }


    public ArrayList<String> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<String> students) {
        this.students = students;
    }

    public ArrayList<String> getAttended() {
        return attended;
    }

    public void setAttended(ArrayList<String> attended) {
        this.attended = attended;
    }
}
