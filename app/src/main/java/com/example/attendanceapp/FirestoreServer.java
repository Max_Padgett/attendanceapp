package com.example.attendanceapp;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.firebase.firestore.SetOptions;
import com.google.firestore.v1.Document;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*This class is a monster of failed ideas honestly. I'll provide a brief description of each method. I recommend collapsing the methods */
public class FirestoreServer {

    private static FirestoreServer instance;    //creating a singleton instance. Firestore already does this but I wanted additional data associated with it.
    FirebaseFirestore db = FirebaseFirestore.getInstance(); //our db instance
    FirebaseAuth auth = FirebaseAuth.getInstance();         //auth instance
    DocumentReference teacher;                              //reference to the teacher if our user is a teacher
    DocumentReference student;                              //reference to the student if our user is a student
    CollectionReference userCurrentCourses;                 //Probably didn't need to save these. They're the current and past courses of the user and school.
    CollectionReference userPastCourses;
    CollectionReference currentCourses;
    CollectionReference pastCourses;
    String user;                                            //Actually used a lot. For checking which type of user is using the app.
    String TAG = "FirestoreServer";


    private FirestoreServer(){

    }
    private FirestoreServer(String user){
        this.user = user;
    }
    //These two methods are used to get and set the user String. Used a bunch for simple checking rather than querying the database more than once.
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }

    //Singleton use
    public static FirestoreServer getInstance(){
        if(instance == null)
            instance = new FirestoreServer();
        return instance;

    }

    //returns a document reference from the students' collection if they exist or we already have done so.
    public DocumentReference getStudent(){
        if(student == null) {
            student = db.collection("Texas A&M University - Corpus Christi")
                    .document("Students").collection("Students")
                    .document(auth.getCurrentUser().getEmail());
        }

        return student;

    }
    //returns a Collection reference from the student's collection of current classes if they exist or we already have done so.
    public CollectionReference getStudentCurrentClasses(){
        if(userCurrentCourses == null)
        {
            userCurrentCourses =  db.collection("Texas A&M University - Corpus Christi")
                    .document("Students").collection("Students").document(auth.getCurrentUser().getEmail())
                    .collection("Current Courses");
        }
        return  userCurrentCourses;

    }
    //returns a Collection reference from the student's collection of past classes if they exist or we already have done so.
    public CollectionReference getStudentPastClasses(){
        if(userPastCourses == null)
        {
            userPastCourses =  db.collection("Texas A&M University - Corpus Christi")
                    .document("Students").collection("Students").document(auth.getCurrentUser().getEmail())
                    .collection("Past Courses");
        }
        return  userPastCourses;
    }

    //returns a document reference from the teachers' collection if they exist or we already have done so.
    public DocumentReference getTeacher(){
        if(teacher == null){
           teacher = db.collection("Texas A&M University - Corpus Christi")
                    .document("Teachers").collection("Teachers")
                    .document(auth.getCurrentUser().getEmail());

        }

        return teacher;
    }
    //returns a Collection reference from the teacher's collection of current classes if they exist or we already have done so.
    public CollectionReference getTeacherCurrentClasses(){
        if(userCurrentCourses == null)
        {
            userCurrentCourses =  db.collection("Texas A&M University - Corpus Christi")
                    .document("Teachers").collection("Teachers").document(auth.getCurrentUser().getEmail())
                    .collection("Current Courses");
        }
        return  userCurrentCourses;

    }
    //returns a Collection reference from the teacher's collection of past classes if they exist or we already have done so.
    public CollectionReference getTeacherPastClasses(){
        if(userPastCourses == null)
        {
            userPastCourses =  db.collection("Texas A&M University - Corpus Christi")
                    .document("Teachers").collection("Teachers").document(auth.getCurrentUser().getEmail())
                    .collection("Past Courses");
        }
        return  userPastCourses;

    }

    //returns a Collection reference from the schools collection of current classes if they exist or we already have done so.
    public CollectionReference getCurrentCourses(){
        if(currentCourses == null){
            currentCourses =  db.collection("Texas A&M University - Corpus Christi")
                    .document("Courses").collection("Current Courses");
        }

        return currentCourses;
    }
    //returns a Collection reference from the schools collection of past classes if they exist or we already have done so.
    //not used because no reason to unless we implement an institution use case
    public CollectionReference getPastCourses(){
        if(pastCourses == null){
            pastCourses =  db.collection("Texas A&M University - Corpus Christi")
                    .document("Courses").collection("Past Courses");
        }

        return pastCourses;
    }

    //returns a reference to a courses document from the schools collection of current classes using the document id. I.E. returns a course in
    //the current curriculum.
    public DocumentReference getCurrentClass(String id){
        return db.collection("Texas A&M University - Corpus Christi")
                .document("Courses").collection("Courses")
                .document(id);
    }

    //add a course from the schools current courses to the teacher or students current courses.
    public void addStudentCourse(DocumentSnapshot documentSnapshot){
        this.getStudentCurrentClasses().document(documentSnapshot.getId()).set(documentSnapshot.getData());
        this.getCurrentCourses().document(documentSnapshot.getId())
                .update("Students", FieldValue.arrayUnion(this.getStudent().getId()));
    }
    public void addTeacherCourse(DocumentSnapshot documentSnapshot){
        this.getTeacherCurrentClasses().document(documentSnapshot.getId()).set(documentSnapshot.getData());
        this.getCurrentCourses().document(documentSnapshot.getId())
                .update("Teacher", getTeacher().getId());
    }

    //return a Collection Refernce to the attendance sheets associated with a particular course.
    public CollectionReference getAttendanceSheets(DocumentReference documentSnapshot){
        return db.collection("Texas A&M University - Corpus Christi")
                .document("Courses").collection("Current Courses")
                .document(documentSnapshot.getId()).collection("Attendance Sheets");
    }
    //returns a Document Reference to the individual attendance sheet defined by its id (the date and time it was created) and the courses id.
    public DocumentReference getAttendanceSheet(String classid, String sheetid){
        return db.collection("Texas A&M University - Corpus Christi")
                .document("Courses").collection("Current Courses")
                .document(classid).collection("Attendance Sheets").document(sheetid);
    }
    //method for adding a new attendance sheet. The document reference passed in is the course's reference. Uses the moveFirestoreDocument method
    //to copy student documents and info from their collection to the All Students Collection for easier access to the students info for attendance
    //managment. I would look at the database to see what I'm talking about here.
    public void addAttendanceSheet(DocumentReference documentReference){
        Task<DocumentSnapshot> task = db.collection("Texas A&M University - Corpus Christi")
                .document("Courses").collection("Current Courses")
                .document(documentReference.getId()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        DocumentSnapshot documentSnapshot = task.getResult();
                        Map<String, Object> attendanceSheet = new HashMap<>();
                        ArrayList<String> students = (ArrayList<String>) documentSnapshot.get("Students");
                        Date timestamp = Timestamp.now().toDate();
                        attendanceSheet.put("id", timestamp);
                        attendanceSheet.put("Attended", new ArrayList<String>());
                        attendanceSheet.put("Students", students);
                        attendanceSheet.put("taking", false);
                        db.collection("Texas A&M University - Corpus Christi")
                                .document("Courses").collection("Current Courses")
                                .document(documentReference.getId()).collection("Attendance Sheets")
                                .document(timestamp.toString()).set(attendanceSheet);
                        if(students != null){
                        for(int i = 0; i < students.size(); i++){
                            DocumentReference toPath =db.collection("Texas A&M University - Corpus Christi")
                                    .document("Courses").collection("Current Courses")
                                    .document(documentReference.getId()).collection("Attendance Sheets")
                                    .document(timestamp.toString()).collection("All Students").document(students.get(i));
                            DocumentReference fromPath = getStudent(students.get(i));
                            moveFirestoreDocument(fromPath, toPath);

                        }
                        }
                    }
                });


    }
    //COPIES firestore documents from one path to another. Also adds two fields in the document it copies to the toPath.
    public void moveFirestoreDocument(DocumentReference fromPath, final DocumentReference toPath) {
        fromPath.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        Map<String, Object> newField = new HashMap<>();
                        newField.put("Attended", false);
                        newField.put("Checked", false);
                        toPath.set(newField, SetOptions.merge());
                        toPath.set(document.getData(), SetOptions.merge())
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error writing document", e);
                                    }
                                });
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }
    //Sets the Attended field to true
    public void updateAttendanceSheet(DocumentSnapshot documentSnapshot){

        documentSnapshot.getReference().update("Attended", true);
    }
    //Sets the Taking field to true or false based on boolean
    public void takingAttendance(DocumentSnapshot documentSnapshot, boolean x){
        if(x)
            documentSnapshot.getReference().update("taking", true);
        else
            documentSnapshot.getReference().update("taking", false);
    }
    //Same as above but uses a reference instead
    public void takingAttendance(DocumentReference documentReference, boolean x){
        if(x)
            documentReference.update("taking", true);
        else
            documentReference.update("taking", false);
    }
    //allows the student to set the checked field to true on their document in the All Students sub-Collection for the particular attendance sheet
    public void studentCheck(DocumentReference documentReference, String id){

        CollectionReference collectionReference = db.collection("Texas A&M University - Corpus Christi")
                .document("Courses").collection("Current Courses")
                .document(documentReference.getId()).collection("Attendance Sheets");
        collectionReference
                .whereEqualTo("taking", true).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String, Object> map = new HashMap<>();
                                map.put("checked", true);
                                collectionReference.document(document.getId()).collection("All Students")
                                        .document(id).set(map, SetOptions.merge());
                            }
                        }
                    }
                });


    }
    //get a student based on string id
    public DocumentReference getStudent(String id){
        return db.collection("Texas A&M University - Corpus Christi")
                    .document("Students").collection("Students")
                    .document(id);
    }
    //not used. Ignore
    public CollectionReference getAllStudentsAttendance(String classid, String sheetid){
        return getAttendanceSheet(classid, sheetid).collection("All Students");
    }


}
