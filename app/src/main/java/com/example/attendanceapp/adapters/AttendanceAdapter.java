package com.example.attendanceapp.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.R;
import com.example.attendanceapp.models.AttendanceModel;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.Date;

/*The recycler view adapter for our attendance sheets.*/
public class AttendanceAdapter extends FirestoreRecyclerAdapter<AttendanceModel, AttendanceAdapter.AttendanceViewHolder> {

    //our on Click listener interface
    OnAttendanceClick listener;

    public AttendanceAdapter(@NonNull FirestoreRecyclerOptions<AttendanceModel> options) {
        super(options);
    }

    //when we bind the viewholder to the UI we call the viewholders set method
    @Override
    protected void onBindViewHolder(@NonNull AttendanceViewHolder attendanceViewHolder, int i, @NonNull AttendanceModel attendanceModel) {
        attendanceViewHolder.setAttendanceName(attendanceModel.getId());
    }

    //inflate the recyclerview
    @NonNull
    @Override
    public AttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_attendance, parent, false);
        return new AttendanceViewHolder(view);
    }
    @Override
    public void onError(FirebaseFirestoreException e) {
        Log.e("error", e.getMessage());
    }

    //the attendance viewholder class sets the data model to the appropriate xml layout stuff.
    class AttendanceViewHolder extends  RecyclerView.ViewHolder {

        private TextView txtName2;
        private CardView mCard;
        View mView;

        public AttendanceViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mCard = itemView.findViewById(R.id.card_view2);
            txtName2 = itemView.findViewById(R.id.textView_recycler2);
            mView.setOnClickListener( new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION && listener != null){
                        listener.attendanceClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });


        }
        void setAttendanceName (Date attendanceName)
        {
            txtName2.setText(attendanceName.toString()); //called in onbindviewholder
        }

    }
    //interface for our on click methods. Needed for click events on individual card views
    public interface OnAttendanceClick{
        void attendanceClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void OnsetAttendanceClick(AttendanceAdapter.OnAttendanceClick listener){
        this.listener = listener;
    }

}
