package com.example.attendanceapp.adapters;

import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.attendanceapp.fragments.CurrentClassFragment;
import com.example.attendanceapp.fragments.PastClassFragment;

/*Used for our tabbed fragments the CurrentClassFragment and PastClassFragment*/
public class ViewPagerFragmentAdapter extends FragmentStateAdapter {

    //required method
    public ViewPagerFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    //also required. returns number of tabs.
    @Override
    public int getItemCount() {
        return 2;
    }

    //returns new fragment based on position
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new CurrentClassFragment();
            case 1:
                return new PastClassFragment();
        }
        return null;
    }
}