package com.example.attendanceapp.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.R;
import com.example.attendanceapp.models.CourseModel;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestoreException;

/*The recycler view adapter for our Current and Past course lists. Implements 2 methods onCreateViewHolder and onBindViewholder*/
public class CourseAdapter extends FirestoreRecyclerAdapter<CourseModel, CourseAdapter.CourseViewHolder> {

    //our on Click listener interface
    OnItemClick listener;

    public CourseAdapter(@NonNull FirestoreRecyclerOptions<CourseModel> options) {
        super(options);
    }


    //inflate the recyclerview from the custom course object to the parent view.
    @NonNull
    @Override
    public CourseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_course, parent, false);
        return new CourseViewHolder(view);
    }
    //when we bind the viewholder to the UI we call the viewholders set method
    @Override
    protected void onBindViewHolder(@NonNull CourseViewHolder courseViewHolder, int i, @NonNull CourseModel courseModel) {
        courseViewHolder.setCourseName(courseModel.getName(), courseModel.getSection(), getSnapshots().getSnapshot(i));

    }
    @Override
    public void onError(FirebaseFirestoreException e) {
        Log.e("error", e.getMessage());
    }

    //the Course viewholder class sets the data model to the appropriate xml layout stuff.
    class CourseViewHolder extends  RecyclerView.ViewHolder {

        private TextView txtName;
        private CardView mCard;
        View mView;

        public CourseViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mCard = itemView.findViewById(R.id.card_view);
            txtName = itemView.findViewById(R.id.textView_recycler);
            //just setting on click listener for the cardview
            mView.setOnClickListener( new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.itemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });


        }
        void setCourseName (String courseName, String section, DocumentSnapshot documentSnapshot)
        {
            txtName.setText(courseName);
        }

    }
    //interface for our on click methods. Needed for click events on individual card views
    public interface OnItemClick{
        void itemClick(DocumentSnapshot documentSnapshot, int position);
    }
    //setting our listener
    public void OnSetItemClick(OnItemClick listener){
        this.listener = listener;
    }
}
