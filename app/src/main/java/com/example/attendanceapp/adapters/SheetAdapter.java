package com.example.attendanceapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.R;
import com.example.attendanceapp.models.SheetModel;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

/*The recycler view adapter for our attendance SHEET. AKA the individual sheets with student names*/
public class SheetAdapter extends FirestoreRecyclerAdapter<SheetModel, SheetAdapter.SheetViewHolder> {

    SheetAdapter.OnSheetClick listener;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public SheetAdapter(@NonNull FirestoreRecyclerOptions<SheetModel> options) {
        super(options);
    }


    //inflate the recyclerview from the custom course object to the parent view.
    @NonNull
    @Override
    public SheetAdapter.SheetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_attendance_item, parent, false);
        return new SheetAdapter.SheetViewHolder(view);
    }

    //when we bind the viewholder to the UI we call the viewholders set method
    @Override
    protected void onBindViewHolder(@NonNull SheetViewHolder sheetViewHolder, int i, @NonNull SheetModel sheetModel) {
        sheetViewHolder.setSheetName(sheetModel.getFirstName(), sheetModel.getLastName(), sheetModel.getAttended(), sheetModel.getChecked());
    }

    //the Sheet viewholder class sets the data model to the appropriate xml layout stuff.
    class SheetViewHolder extends  RecyclerView.ViewHolder {

        TextView txtName2;
        CardView mCard;
        ImageView imageView;
        View mView;

        public SheetViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            imageView = itemView.findViewById(R.id.checkmark);
            mCard = itemView.findViewById(R.id.card_view3);
            txtName2 = itemView.findViewById(R.id.textView_recycler3);
            //just setting on click listener for the cardview
            mView.setOnClickListener( new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION && listener != null){
                        listener.sheetClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });


        }

        //This method is more complex than the other adapters. Basically on the documents in Attendance Sheets/All Students/%student document% their are
        //two booleans a attended boolean and checked boolean. If the attended bool is true they get a green check, else if they've checked in a yellow circle,
        //finnally if they haven't done either they have a red x.
        void setSheetName (String sheetfName,String sheetlName, Boolean attended, Boolean checked)
        {
            txtName2.setText(sheetfName + " " + sheetlName);
            if(attended){
                imageView.setImageResource(R.drawable.ic_baseline_check_24);
            }else if(checked)
            {
                imageView.setImageResource(R.drawable.ic_baseline_brightness_1_24);
            }else
            {
                imageView.setImageResource(R.drawable.ic_baseline_close_24);
            }
        }

    }

    //interface for our on click methods. Needed for click events on individual card views
    public interface OnSheetClick{
        void sheetClick(DocumentSnapshot documentSnapshot, int position);
    }
    //setting our listener
    public void OnsetSheetClick(SheetAdapter.OnSheetClick listener){
        this.listener = listener;
    }
}
